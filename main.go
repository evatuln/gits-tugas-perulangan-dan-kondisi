package main

import (
	"fmt"
)

func main() {
	var num, choice int

	fmt.Println("======JENIS SEGITIGA======")
	fmt.Println("1. Normal")
	fmt.Println("2. Terbalik")
	fmt.Println("=======================")
	fmt.Printf("Pilih Jenis Segitiga : ")
	fmt.Scan(&choice)
	switch choice {
	case 1:
		fmt.Printf("Masukkan Jumlah Bintang : ")
		fmt.Scan(&num)
		for i := 1; i <= num; i++ {
			for j := num; j >= i; j-- {
				fmt.Print(" ")
			}
			for z := 1; z <= i; z++ {
				fmt.Print("*")
			}
			for a := 1; a <= i-1; a++ {
				fmt.Print("*")
			}
			fmt.Println()
		}
	case 2:
		fmt.Printf("Masukkan Jumlah Bintang : ")
		fmt.Scan(&num)
		for i := 0; i <= num; i++ {
			for j := 0; j <= i; j++ {
				fmt.Print(" ")
			}
			for z := 0; z >= (i - num); z-- {
				fmt.Print("*")
			}
			for z := 0; z < (num - i); z++ {
				fmt.Print("*")
			}
			fmt.Println()
		}
	default:
		fmt.Println("Wrong input")
	}

}
